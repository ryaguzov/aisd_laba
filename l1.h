#pragma once
#include <iostream>
#include "Array.h"

typedef int TYPE;

void DrawMenu()
{
	std::cout << "0 - ������ �����������" << std::endl
		<< "1 - ����������� � �������� ��������" << std::endl
		<< "2 - ����������� � �������� �������� � ��������� �� ���������" << std::endl
		<< "3 - �������� �������" << std::endl
		<< "4 - �������� �������" << std::endl
		<< "5 - ������� �������" << std::endl
		<< "6 - ������� ������� �� �������" << std::endl
		<< "7 - At()" << std::endl
		<< "8 - []" << std::endl
		<< "9 - ������ �������" << std::endl
		<< "10 - ��������" << std::endl
		<< "11 - ������" << std::endl
		<< "12 - ��������" << std::endl
		<< "13 - ������" << std::endl
		<< "14 - �������" << std::endl
		<< "15 - ���������� �������" << std::endl
		<< "16 - ���������� � ������� ���������" << std::endl
		<< "17 - ���������� � �������� ������� � ������� ���������" << std::endl

		<< "-1 - Exit" << std::endl
		<< "-2 - Clear screen" << std::endl;
}


Array<TYPE>* arr;

bool CompareArray(TYPE* exp, Array<TYPE>* orig)
{
	for (int i = 0; i < 5; i++)
	{
		if ((*orig)[i] != exp[i])
			return false;
	}

	return true;
}

int TestCaseInit()
{
	arr = new Array<TYPE>();
	return 0;
}

int TestCaseInitCapacity(int cap)
{
	arr = new Array<TYPE>(cap);
	return 0;
}

int TestCaseInitDefaultValue(TYPE value, int cap)
{
	arr = new Array<TYPE>(cap, value);
	return 0;
}

int TestPushBack()
{
	std::cout << "TestPushBack" << std::endl;
	TYPE expArray[5] = { 0,1,2,3,4 };
	TestCaseInit();
	for (size_t i = 0; i < 5; i++)
	{
		arr->PushBack(i);
	}

	if (!CompareArray(expArray, arr)) return 1;
	delete arr;
	return 0;
}

int TestInsert()
{
	std::cout << "TestInsert" << std::endl;

	TYPE expArray[6] = { 0,1,2,10,3,4 };
	TestCaseInit();
	for (size_t i = 0; i < 5; i++)
	{
		arr->PushBack(i);
	}
	arr->Insert(10, 2);

	if (CompareArray(expArray, arr)) return 1;
	delete arr;
	return 0;
}

int TestSize(int size)
{
	std::cout << "TestSize" << std::endl;

	TestCaseInitDefaultValue(1, size);
	if (arr->GetSize() != size) return 1;
	delete arr;
	return 0;
}

int TestGetCapacity(int capacity)
{
	std::cout << "TestGetCapacity" << std::endl;

	TestCaseInitCapacity(capacity);
	if (arr->GetCapacity() != capacity) return 1;
	delete arr;
	return 0;
}

int TestSetCapacity(int capacity)
{
	std::cout << "TestSetCapacity" << std::endl;

	TestCaseInit();
	arr->SetCapacity(capacity);

	if (arr->GetCapacity() != capacity) return 1;
	delete arr;
	return 0;
}

int ShrinkTest()
{
	std::cout << "ShrinkTest" << std::endl;

	TestCaseInit();
	for (size_t i = 0; i < 5; i++)
	{
		arr->PushBack(i);
	}

	int currentCapacity = arr->GetCapacity();

	arr->shrink_to_fit();

	if (currentCapacity == arr->GetCapacity()) return 1;
	delete arr;
	return 0;
}

int ClearTest()
{
	std::cout << "ClearTest" << std::endl;

	TestCaseInit();
	for (size_t i = 0; i < 5; i++)
	{
		arr->PushBack(i);
	}

	int currentCapacity = arr->GetCapacity();
	int currentSize = arr->GetSize();

	arr->Clear();

	if (currentCapacity == arr->GetCapacity()) return 1;
	if (currentSize == arr->GetSize()) return 1;
	delete arr;
	return 0;
}

int TestIsEmpty()
{
	std::cout << "TestIsEmpty" << std::endl;

	TestCaseInit();

	if (!arr->IsEmpty()) return 1;

	for (size_t i = 0; i < 5; i++)
	{
		arr->PushBack(i);
	}

	if (arr->IsEmpty()) return 1;
	delete arr;
	return 0;
}

int TestContains()
{
	std::cout << "TestContains" << std::endl;

	TestCaseInit();
	for (size_t i = 0; i < 5; i++)
	{
		arr->PushBack(i);
	}

	if (!arr->Contains(2)) return 1;
	delete arr;
	return 0;
}

int TestAt()
{
	std::cout << "TestAt" << std::endl;

	TestCaseInit();
	for (size_t i = 0; i < 5; i++)
	{
		arr->PushBack(i);
	}

	if (arr->At(1) != 1) return 1;
	delete arr;
	return 0;
}

int TestGetIndexOf()
{
	std::cout << "TestGetIndexOf" << std::endl;

	TestCaseInit();
	for (size_t i = 0; i < 5; i++)
	{
		arr->PushBack(i);
	}

	if (arr->GetIndexOf(2) != 2) return 1;
	delete arr;
	return 0;
}

int TestRemove()
{
	std::cout << "TestRemove" << std::endl;

	TestCaseInit();
	for (size_t i = 0; i < 5; i++)
	{
		arr->PushBack(i);
	}

	if (arr->Remove(7)) return 1;
	if (!arr->Remove(4)) return 1;
	if (arr->Contains(4)) return 1;
	delete arr;
	return 0;
}

int TestRemoveByIndex()
{
	std::cout << "TestRemoveByIndex" << std::endl;

	TestCaseInit();
	for (size_t i = 0; i < 5; i++)
	{
		arr->PushBack(i);
	}

	int removedItem = arr->At(1);
	if (arr->RemoveByIndex(7)) return 1;
	if (!arr->RemoveByIndex(1)) return 1;
	if ((*arr)[1] == removedItem) return 1;
	delete arr;
	return 0;
}

void RunTests()
{
	TestCaseInit();
	delete arr;
	TestCaseInitCapacity(5);
	delete arr;
	TestCaseInitDefaultValue(1, 5);
	delete arr;

	std::cout << TestPushBack() << std::endl;
	std::cout << TestInsert() << std::endl;
	std::cout << TestSize(5) << std::endl;
	std::cout << TestGetCapacity(11) << std::endl;
	std::cout << TestSetCapacity(11) << std::endl;
	std::cout << ShrinkTest() << std::endl;
	std::cout << ClearTest() << std::endl;
	std::cout << TestIsEmpty() << std::endl;
	std::cout << TestContains() << std::endl;
	std::cout << TestAt() << std::endl;
	std::cout << TestGetIndexOf() << std::endl;
	std::cout << TestRemove() << std::endl;
	std::cout << TestRemoveByIndex() << std::endl;
}


void RunL1()
{
	RunTests();

	setlocale(LC_ALL, "rus");
	DrawMenu();

	arr = new Array<TYPE>(5, 2);

	int menuInput = 1;
	while (menuInput != -1)
	{
		try
		{
			std::cout << std::endl << "Command: ";
			std::cin >> menuInput;
			std::cout << std::endl;

			switch (menuInput)
			{
			case 0:
				arr = new Array<TYPE>();
				break;
			case 1:
			{
				int capacity;

				std::cout << "Input capacity: ";
				std::cin >> capacity;
				std::cout << std::endl;

				arr = new Array<TYPE>(capacity);
				break;
			}
			case 2:
			{
				int capacity;
				TYPE item;

				std::cout << "Input capacity: ";
				std::cin >> capacity;
				std::cout << std::endl << "Input default value: ";
				std::cin >> item;
				std::cout << std::endl;

				arr = new Array<TYPE>(capacity, item);
				break;
			}
			case 3:
			{
				TYPE item;
				std::cout << "Input item: ";
				std::cin >> item;
				std::cout << std::endl;
				arr->PushBack(item);
				break;
			}
			case 4:
			{
				if (arr == nullptr)
				{
					std::cout << "Init array!" << std::endl;
					break;
				}
				int index;
				TYPE item;
				std::cout << "Input item: ";
				std::cin >> item;
				std::cout << "Input insert index: ";
				std::cin >> index;
				std::cout << std::endl;

				arr->Insert(item, index);
				break;
			}
			case 5:
			{
				if (arr == nullptr)
				{
					std::cout << "Init array!" << std::endl;
					break;
				}
				TYPE item;
				std::cout << "Input item to remove: ";
				std::cin >> item;
				std::cout << std::endl;

				if (arr->Remove(item))
					std::cout << "item " << item << " removed" << std::endl;
				else
					std::cout << "item " << item << " didn't removed" << std::endl;
				break;
			}
			case 6:
			{
				if (arr == nullptr)
				{
					std::cout << "Init array!" << std::endl;
					break;
				}
				int index;
				std::cout << std::endl;
				std::cout << "Input remove index: ";
				std::cin >> index;
				std::cout << std::endl;

				if (arr->RemoveByIndex(index))
					std::cout << "item at index " << index << " removed" << std::endl;
				else
					std::cout << "item at index " << index << " didn't removed" << std::endl;
				break;
			}
			case 7:
			{
				if (arr == nullptr)
				{
					std::cout << "Init array!" << std::endl;
					break;
				}
				int index;
				std::cout << std::endl;
				std::cout << "Input index to get by At(): ";
				std::cin >> index;
				std::cout << std::endl << arr->At(index) << std::endl;
				break;
			}
			case 8: {
				if (arr == nullptr)
				{
					std::cout << "Init array!" << std::endl;
					break;
				}
				int index;
				std::cout << std::endl;
				std::cout << "Input index to get by operator[]: ";
				std::cin >> index;
				std::cout << std::endl
					<< (*arr)[index] << std::endl;
				break;
			}
			case 9: {
				if (arr == nullptr)
				{
					std::cout << "Init array!" << std::endl;
					break;
				}
				TYPE item;
				std::cout << std::endl;
				std::cout << "Input item to get index of: ";
				std::cin >> item;
				std::cout << std::endl
					<< "index of " << item << " is " << arr->GetIndexOf(item) << std::endl;
				break;
			}
			case 10: {
				if (arr == nullptr)
				{
					std::cout << "Init array!" << std::endl;
					break;
				}
				TYPE item;
				std::cout << std::endl;
				std::cout << "Input item to check contains: ";
				std::cin >> item;
				std::cout << std::endl;
				if (arr->Contains(item))
					std::cout << "Collection contains " << item << std::endl;
				else
					std::cout << "Collection doesn't contains " << item << std::endl;
				break;
			}
			case 11: {
				if (arr == nullptr)
				{
					std::cout << "Init array!" << std::endl;
					break;
				}
				std::cout << "Collection's size - " << arr->GetSize() << std::endl;

				break;
			}
			case 12: {
				if (arr == nullptr)
				{
					std::cout << "Init array!" << std::endl;
					break;
				}
				arr->Clear();
				std::cout << "Cleared" << std::endl;

				break;
			}

			case 13: {
				if (arr == nullptr)
				{
					std::cout << "Init array!" << std::endl;
					break;
				}
				if (arr->IsEmpty())
					std::cout << "Collection is empty" << std::endl;
				else
					std::cout << "Collection isn't empty" << std::endl;

				break;
			}

			case 14: {
				if (arr == nullptr)
				{
					std::cout << "Init array!" << std::endl;
					break;
				}
				std::cout << "Collection's capacity - " << arr->GetCapacity() << std::endl;

				break;
			}
			case 15: {
				if (arr == nullptr)
				{
					std::cout << "Init array!" << std::endl;
					break;
				}
				int capacity;
				std::cout << std::endl;
				std::cout << "Input new capacity: ";
				std::cin >> capacity;
				std::cout << std::endl;
				arr->SetCapacity(capacity);

				std::cout << "New capacity - " << arr->GetCapacity() << std::endl;
				break;
			}

			case 16:
			{
				if (arr == nullptr)
				{
					std::cout << "Init array!" << std::endl;
					break;
				}

				for (auto iter = arr->Begin(); iter != arr->End(); iter++)
				{
					std::cout << *iter << " ";
				}
				break;
			}

			case 17:
			{
				if (arr == nullptr)
				{
					std::cout << "Init array!" << std::endl;
					break;
				}
				for (auto iter = arr->RBegin(); iter != arr->REnd(); iter--)
				{
					std::cout << *iter << " ";
				}
				break;
			}
			case 18:
			{
				int iterMenuInput = 0;
				Iterator<TYPE> iter = nullptr;
				std::cout << "0 - Begin" << std::endl;
				std::cout << "1 - End" << std::endl;
				std::cout << "2 - RBegin" << std::endl;
				std::cout << "3 - REnd" << std::endl;
				std::cout << "4 - ++" << std::endl;
				std::cout << "5 - --" << std::endl;
				std::cin >> iterMenuInput;
				while (iterMenuInput != -1)
				{

					switch (iterMenuInput)
					{
					case 0:
						iter = arr->Begin();
						std::cout << *iter << std::endl;
						break;
					case 1:
						iter = arr->End();
						std::cout << *iter << std::endl;
						break;
					case 2:
						iter = arr->RBegin();
						std::cout << *iter << std::endl;
						break;
					case 3:
						iter = arr->REnd();
						std::cout << *iter << std::endl;
						break;
					case 4:
						++iter;
						std::cout << *iter << std::endl;
						break;
					case 5:
						--iter;
						std::cout << *iter << std::endl;
						break;
					}

					std::cin >> iterMenuInput;
				}
			}
			case -2:
				system("CLS");
				DrawMenu();
				break;

			case -1:
			default:
				break;
			}
		}
		catch (std::exception exp)
		{
			std::cout << exp.what() << std::endl;
		}
	}
}