#pragma once

template <typename T>
class Iterator
{
private:
	T* m_current;

public:
	Iterator(T* start)
	{
		m_current = start;
	}

	T& operator*() { return *m_current; }
	T& operator++(int)
	{
		return *m_current++;
	}
	T& operator++()
	{
		return *++m_current;
	}

	T& operator--(int)
	{
		return *m_current--;
	}

	T& operator--()
	{
		return *--m_current;
	}
	T& operator=(T* obj)
	{
		m_current = obj;
		return &(*m_current);
	}

	bool operator==(const Iterator& obj) { return m_current == obj.m_current; }
	bool operator!=(const Iterator& obj) { return m_current != obj.m_current; }
	bool operator<(const Iterator& obj) { return m_current < obj.m_current; }
	bool operator>(const Iterator& obj) { return m_current > obj.m_current; }
};

template <typename T>
class Array
{
	int m_capacity;
	int m_size = 0;
	T *m_array;
	
public:
	Array<T>(int capacity = 3)
	{
		m_array = new T[capacity];
		m_capacity = capacity;
	}
	Array<T>(int capacity, T val)
	{
		m_array = new T[capacity];
		
		for (int i = 0; i < capacity; i++)
			m_array[i] = val;

		for (int i = 0; i < capacity; i++)
			T t = m_array[i];
		m_capacity = capacity;
		m_size = m_capacity;
	}
	Array<T>(Array& copy)
	{
		m_size = copy.m_size;
		m_capacity = copy.m_capacity;

		m_array = new T[m_capacity];
		Copy(copy.m_array, m_array);
	}
	
	~Array<T>()
	{
		delete[] m_array;
	}

public:
	friend Iterator<T>;
	
	int GetSize() const {return m_size;}
	int GetCapacity() const { return m_capacity; }
	void SetCapacity(int capacity)
	{
		if (capacity < m_size)
			throw std::exception("Capacity can not be lower the size array");
		
		Resize(capacity);
	}
	
	void shrink_to_fit()
	{
		Resize(m_size);
	}

	void Clear()
	{
		m_size = 0;
		m_capacity = 3;
		delete[] m_array;
		m_array = new T[m_capacity];
	}
	bool IsEmpty() const { return m_size == 0; }

	bool Contains(T item) const
	{
		for(size_t i = 0; i < m_size; i++)
			if (m_array[i] == item) 
				return  true;
		
		return  false;
	}
	
	T At(const int index) const { return  m_array[index]; }
	int GetIndexOf(T item) const
	{
		for (size_t i = 0; i < m_size; i++)
			if (m_array[i] == item) return i;

		return -1;
	}
	
	void PushBack(T item)
	{
		if(m_size == m_capacity)
		{
			Resize();
		}
		
		m_array[m_size] = item;
		m_size++;
	}
	void Insert(T item, int index)
	{
		if (m_size == m_capacity)
		{
			Resize();
			InsertInternal(item, index);
		}
		else
		{
			InsertInternal(item, index);
		}
	}

	bool Remove(T item)
	{
		int index = GetIndexOf(item);
		return RemoveByIndex(index);
		
	}
	bool RemoveByIndex(int index)
	{
		if (index >= 0 && index < m_size)
		{
			m_size--;
			T* temp = new T[m_capacity];

			for (int i = 0; i < index; i++)
				temp[i] = m_array[i];
			
			for (int i = index; i < m_size+1; i++)
				temp[i] = m_array[i + 1];
			
			delete[] m_array;
			m_array = temp;
			return true;
		}

		return false;
	}

	T& operator[] (const int index) const { return m_array[index]; }

	Iterator<T>& Begin()
	{
		if(m_size > 0)
			return *(new Iterator<T>(m_array));
		return End();
	}
	Iterator<T>& End() { return *(new Iterator<T>(m_array + m_size)); }

	Iterator<T>& RBegin()
	{
		if(m_size > 0)
			return *(new Iterator<T>(m_array + m_size - 1));
		return REnd();
	}
	Iterator<T>& REnd() { return *(new Iterator<T>(m_array-1)); }
	
	void Print()
	{
		for (size_t i = 0; i < m_size; i++)
			std::cout << m_array[i] << " ";
		std::cout << std::endl;
	}
private:
	void InsertInternal(T item, int index)
	{
		if (index != m_size)
		{
			T nextValue = m_array[index];
			m_array[index] = item;
			m_size++;

			T curValue;
			for (size_t i = index + 1; i < m_size; i++)
			{
				curValue = m_array[i];
				m_array[i] = nextValue;
				nextValue = curValue;
			}
		}
		else
		{
			PushBack(item);
		}
	}

	void Resize(int capacity = -1)
	{
		if (capacity == -1)
			capacity = m_capacity * 2;

		m_capacity = capacity;
		T* newArray = new T[m_capacity];

		Copy(m_array, newArray);
		delete[] m_array;
		m_array = newArray;
	}

	void Copy( T*& from, T*& to)
	{
		for (size_t i = 0; i < m_size; i++)
		{
			to[i] = from[i];
		}
	}
};


